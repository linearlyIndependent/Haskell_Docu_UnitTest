# **Tips, Commands and other useful stuff**

--------------------------------------------
*I wrote it in English because everything in IT is in English.*

**Take a look, maybe you will find something useful.**

Oh and never **ever** try to work with Cabal or Stack on Windows. 

**It's a mess, use Linux instead!**

--------------------------------------------

# Professional literature:
[Real World Haskell, Chapter 11](http://book.realworldhaskell.org/read/testing-and-quality-assurance.html)

[Beginning Haskell, Chapter 2 from site 22 && Chapter 15](https://www.amazon.com/Beginning-Haskell-Alejandro-Serrano-Mena/dp/1430262508)

# 1. install Stack with:
```bash
curl -sSL https://get.haskellstack.org/ | sh
```

# 2. Switch from Amazon-Proxy to the real Stackage server

If the stack download through the standard Amazon-Proxy is taking **literally** ages, then you can try the following tweak:

[https://github.com/commercialhaskell/stack/issues/3088](https://github.com/commercialhaskell/stack/issues/3088)

[https://docs.haskellstack.org/en/stable/yaml_configuration/](https://docs.haskellstack.org/en/stable/yaml_configuration/)

The solution:
```bash
# copy the following into /home/USER/.stack/config.yaml
#
#package-indices:
#- name: HackageOrig
#  download-prefix: https://hackage.haskell.org/package/
#  http: https://hackage.haskell.org/01-index.tar.gz
```

# 3. Cabal, Stack and creating Stack-Projects
[https://docs.haskellstack.org/en/stable/GUIDE/](https://docs.haskellstack.org/en/stable/GUIDE/)

[https://docs.haskellstack.org/en/stable/GUIDE/#stackyaml-vs-cabal-files](https://docs.haskellstack.org/en/stable/GUIDE/#stackyaml-vs-cabal-files)

[https://www.fpcomplete.com/blog/2015/06/why-is-stack-not-cabal](https://www.fpcomplete.com/blog/2015/06/why-is-stack-not-cabal)

[https://haskell-lang.org/tutorial/stack-build](https://haskell-lang.org/tutorial/stack-build)

[http://seanhess.github.io/2015/08/04/practical-haskell-getting-started.html](http://seanhess.github.io/2015/08/04/practical-haskell-getting-started.html)


## Edit global author-info (global for all your projects)
It's in: /home/USER/.stack/config.yaml

## Some commands:
```bash
stack templates                 # see available templates
stack new my-project [template] # create project
stack ls dependencies           # list dependencies
stack ghci                      # start ghci in context of the package -> load project when starting
stack setup                     # creates the project's sandbox, only run it once at project creation 
stack build                     # build the project
stack solver                    # calculate build dependencies new, you only need it when you have some build conflicts
stack exec my-project-exe       # run exe
stack install my-project        # copy executable to defined directory (~install it)
    my-project-exe              # then run it normally from the console

find * -type f                  # list files and folders
```

## Config-Files

**Project-wide** settings are at: /project/stack.yaml

**Package-wide** settings are at: /project/PackageName.cabal

# 4. Documentation (Haddock, etc.)
The generated documentary is located at:

/ProjectName/.stack-work/install/i386-linux/lts-11.8/8.2.2/doc/index.html

* So you don't have to search through all subdirectories ;) *

```bash
cd my-project                                  # switch to project dir
stack dot --external | dot -Tpng -o wreq.png   # visualize dependencies (internal & external)
cabal haddock --haddock-option=--latex         # build latex with cabal
stack haddock my-project                       # build documentation (html)
```

# 5. HLINT (syntax, scemantics checker)
```bash
stack install hlint                            # install hlint (takes really long)
hlint <full filename>                          # analyze a .hs file
```

# 6. Dependencies
```bash
stack ls dependencies                          # list dependencies of project
stack dot --external | dot -Tpng -o wreq.png   # dependencies, --external stands for external dependencies
```
[https://docs.haskellstack.org/en/stable/dependency_visualization/](https://docs.haskellstack.org/en/stable/dependency_visualization/)

# 7. Tasty

**Testtree**: A recursive tree-structure which

consists of **TestGroups**
  
which consist either of **TestCase** -s
    
or of **TestGroups** -s
   
    
```bash
stack test                                      # Run all tests
stack test --test-arguments -l                  # List all tests
stack test --test-arguments "-j n"              # Run tests on **n** threads parallel
```

[https://github.com/feuerbach/smallcheck/wiki/Comparison-with-QuickCheck](https://github.com/feuerbach/smallcheck/wiki/Comparison-with-QuickCheck)