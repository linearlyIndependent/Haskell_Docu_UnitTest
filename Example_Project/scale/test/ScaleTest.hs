module Main where

import System.IO.Unsafe

-- | Import "Tasty", and the packages for the used test-framework
import Test.Tasty
import Test.Tasty.SmallCheck as SC
import Test.Tasty.QuickCheck as QC
import Test.Tasty.HUnit as HU
import Test.Tasty.Hspec as HS

-- Import the modules which you wish to test
import Scale as Sa
import Scalable.Ingredient as In
import Example.SmallExamp


main = defaultMain scaleTests

-- | The root of the 'TestTree'
scaleTests :: TestTree                 -- TestGroups
scaleTests = testGroup "All Scale Tests" [miniTests, addIngredientTests]

miniTests :: TestTree
miniTests = testGroup "Some Small Tests" [unit_miniTests, spec_miniTests, prop_miniTests, scprop_miniTests]

unit_miniTests = testGroup "[HUnit] - Testing myReverse"
    [-- | TestCases
       HU.testCase " Reverse List: []" $
        HU.assertEqual "failed: not equal" ([] :: [Int]) (myReverse . myReverse $ ([] :: [Int]) )
     
     , HU.testCase " Reverse List: [2]" $
          HU.assertEqual "failed: not equal" [2] (myReverse . myReverse $ [2])
          
     , HU.testCase " Reverse List: [1,2,3,4]" $
          HU.assertEqual "failed: not equal" [1,2,3,4] (myReverse . myReverse $ [1,2,3,4])           
    ]

              -- Don't worry about 'unsafePerformIO', according to the official documentation this is completely safe.
spec_miniTests = unsafePerformIO $ testSpec "[Hspec] - Testing myReverse" $ do 

       it "Reverses the list twice: []" $
            (myReverse . myReverse $ ([] :: [Int]) ) `shouldBe` ([] :: [Int])
            
       it "Reverses the list twice: []" $
            (myReverse . myReverse $ [2])  `shouldBe` [2]
            
       it "Reverses the list twice: []" $
            (myReverse . myReverse $ [1,2,3,4])  `shouldBe` [1,2,3,4]

prop_miniTests = testGroup "[QuickCheck] - Testing myReverse"
    [ 

       QC.testProperty " fail: reversing the list twice" $
         \(testList) -> (testList :: [Char]) == (myReverse . myReverse $ (testList :: [Char]))   
 
    ] 

      
scprop_miniTests = testGroup "[SmallCheck] - Testing myReverse"
    [ 
       SC.testProperty " fail: reversing the list twice" $
         \(testList) -> (testList :: [Char]) == (myReverse . myReverse $ (testList :: [Char])) 
    ] 

-----------------------------

addIngredientTests :: TestTree
addIngredientTests = testGroup "Add Ingredients Tests" [unit_addIngredient, spec_addIngredient, prop_addIngredient]


unit_addIngredient = testGroup "[HUnit] - Adding ingredients into the scale"
    [
       HU.testCase "Insert 0 kg into empty scale" $
          HU.assertEqual "failed: not equal" (Sa.KitchenScale 0 0) (addIngredient Sa.newScale $ ingredientWithMass "flour" 0)
     
     , HU.testCase "Insert 3 kg into empty scale" $
          HU.assertEqual "failed: not equal" (Sa.KitchenScale 3 3) (addIngredient Sa.newScale $ ingredientWithMass "flour" 3)
     -- is supposed to fail:               
     , HU.testCase "Remove 3 kg from empty scale" $
          HU.assertEqual "failed: not equal" (Sa.KitchenScale (-3) (-3)) (addIngredient Sa.newScale $ ingredientWithMass "flour" (-3))
{-   -- this not:                 
     , HU.testCase "insert (-3) kg into empty scale" $
          HU.assertEqual "not equal" (Sa.KitchenScale 0 0) (addIngredient Sa.newScale $ ingredientWithMass "flour" (-3))
-}          
     , HU.testCase "Remove 3 kg from scale with 4 kg, disp. 2 kg" $
          HU.assertEqual "failed: not equal" (Sa.KitchenScale 1 (-1)) (addIngredient (Sa.KitchenScale 4 2) $ ingredientWithMass "flour" (-3))          

     , HU.testCase "Remove 3 kg from scale with 2 kg, disp. 2 kg" $
          HU.assertEqual "failed: not equal" (Sa.KitchenScale 0 0) (addIngredient (Sa.KitchenScale 2 2) $ ingredientWithMass "flour" (-3))          
    ]
    
spec_addIngredient = unsafePerformIO $ testSpec "[Hspec] - Adding ingredients into the scale" $ do 

       it "Inserts 0 kg correctly into an empty scale" $
            (addIngredient Sa.newScale $ ingredientWithMass "flour" 0) `shouldBe` Sa.KitchenScale 0 0
            
       it  "Inserts 3 kg correctly into an empty scale" $
            (addIngredient Sa.newScale $ ingredientWithMass "flour" 3) `shouldBe` Sa.KitchenScale 3 3    
     -- is supposed to fail                   
       it  "Inserts 3 kg correctly into an empty scale" $
            (addIngredient Sa.newScale $ ingredientWithMass "flour" (-3)) `shouldBe` Sa.KitchenScale (-3) (-3)
{-   -- this not:                  
       it  "Inserts 3 kg correctly into an empty scale" $
            (addIngredient Sa.newScale $ ingredientWithMass "flour" (-3)) `shouldBe` Sa.KitchenScale 0 0      
-}
       it  "Removes 3 kg correctly from scale with 4 kg, displaying 2 kg" $
            (addIngredient (Sa.KitchenScale 4 2) $ ingredientWithMass "flour" (-3)) `shouldBe` Sa.KitchenScale 1 (-1) 
            
       it  "Removes 3 kg correctly from scale with 2 kg, displaying 2 kg" $
            (addIngredient (Sa.KitchenScale 2 2) $ ingredientWithMass "flour" (-3)) `shouldBe` Sa.KitchenScale 0 0       

--instance Arbitrary Scale

prop_addIngredient = testGroup "[QuickCheck] - Adding ingredients into the scale"
    [ 
    -- | Fails after some tries because it also generates negative values which have extra conditions.
       QC.testProperty " fail: add weights into scale" $
         \(newMass) -> (Sa.KitchenScale newMass newMass) == (addIngredient (Sa.newScale) $ ingredientWithMass "flour" (newMass))       

    -- | This is much more convenient:
    --   Here you use your own generator to generate test data.
    --   This checks also only for positive values.
     , QC.testProperty " fail: add weights into scale" $
         \(Positive newMass) (scale) -> (Sa.KitchenScale (newMass + Sa.mass scale) (newMass + Sa.dispMass scale)) == (addIngredient scale $ ingredientWithMass "flour" (abs newMass))       
      ] 

-- | You have to define a generator which generates test instances if you want to test your own types.
instance Arbitrary (Sa.KitchenScale) where
    arbitrary = do
      Positive mass <- arbitrary                                -- Generate only positive values
      dispMass <- suchThat arbitrary (\a -> a <= mass && a > 0) -- Values should be ]0;mass]
      return (Sa.KitchenScale mass dispMass)

-- | You can chain generators too.
--   Float -> 'KitchenScale'-> 'ScaleTest'
instance Arbitrary (ScaleTest) where
    arbitrary = do
      testScale <- arbitrary
      testMass <- arbitrary --suchThat arbitrary (\a -> )
      return (ScaleTest testScale testMass)

-- | Just some internal type to generate custom test data.
data ScaleTest = ScaleTest 
                {
                   testScale :: Sa.KitchenScale
                ,  testMass :: Float
                }
                deriving (Show) 