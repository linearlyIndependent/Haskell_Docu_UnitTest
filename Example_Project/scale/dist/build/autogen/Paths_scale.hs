{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_scale (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/p1tr4k/.cabal/bin"
libdir     = "/home/p1tr4k/.cabal/lib/i386-linux-ghc-8.4.2/scale-0.1.0.0-AjhCHeHg1wc9CimeHVGtTw"
dynlibdir  = "/home/p1tr4k/.cabal/lib/i386-linux-ghc-8.4.2"
datadir    = "/home/p1tr4k/.cabal/share/i386-linux-ghc-8.4.2/scale-0.1.0.0"
libexecdir = "/home/p1tr4k/.cabal/libexec/i386-linux-ghc-8.4.2/scale-0.1.0.0"
sysconfdir = "/home/p1tr4k/.cabal/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "scale_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "scale_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "scale_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "scale_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "scale_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "scale_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
