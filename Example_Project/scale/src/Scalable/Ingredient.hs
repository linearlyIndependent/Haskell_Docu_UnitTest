{-| 
Module      : Scalable.Ingredient
Description : Holds the 'CookingIngredient' type and some functions.
Copyright   : Do whatever you want.
License     : GPL-3
Maintainer  : patrik.ploechl@fake.com
Stability   : Experimental
Portability : POSIX

This /module/ provides the ingredients to be wighted on the "Scale".

 * Uses the 'Convert.Converter' Module.
-}
module Scalable.Ingredient(-- 1. Custom types
                            CookingIngredient(CookingIngredient)
                           --     * Lookup functions
                          , name
                          , mass
                           -- (2) Creating a 'CookingIngredient'
                           --     * Define by mass
                          , ingredientWithMass
                           --     * Define by volume
                          , ingredientWithVolume                           
                           ) where

-- Imports
import qualified Convert.Converter as Co
import qualified Data.Maybe as My


-- | The type of the cooking ingredients.
--
--  * Unit of measurement is mass (kg).
data CookingIngredient
                      -- | The standard constructor.
                      = CookingIngredient { -- | The name of the ingredient.
                                             name :: String
                                           , mass :: Float -- ^ The mass of the ingredient.
                                           }
                         deriving (Show, Eq)

instance Ord CookingIngredient where 
         compare c1 c2 = compare (mass c1) (mass c2) 

{-|
Creates a 'CookingIngredient' which is defined by its mass (kg).

 * Negative negative mass is converted to 0.
 
 @
    someFunc x = x + x
 @
 
 > someFunc2 x = x + x
 
>>> ingredientWithMass "milk" 2.0
CookingIngredient {name = "milk", mass = 2.0}

>>> ingredientWithMass "sulphur" 2.0
CookingIngredient {name = "sulphur", mass = 2.0}
-}
ingredientWithMass :: [Char] -> Float -> CookingIngredient
ingredientWithMass = CookingIngredient

{-|
Creates a 'CookingIngredient' defined by its volume, not its mass.

 * Internally the volume will be converted to mass.
 * Negative negative volume / mass is converted to 0. 
 * Uses the values in the 'Co.densityMap' for conversion.
     (1) Returns a 'CookingIngredient' if it is defined the 'Co.densityMap'. 
     2. Else it throws an 'base.Exception'!
 
>>> ingredientWithVolume "milk" 2.0
CookingIngredient {name = "milk", mass = 2.12}

>>> ingredientWithVolume "sulphur" 2.0
Exception.
-}
ingredientWithVolume :: [Char] -> Float -> CookingIngredient
ingredientWithVolume name volume = CookingIngredient name $ max 0 $ My.fromJust $ Co.dm3ToKg name volume