module Example.SmallExamp(
                         myReverse
                         ) where
                         
-- | Reverses a list, used to demonstrate some tests.                         
myReverse :: [a] -> [a]
myReverse list = if length list == 2 
                    then [] 
                    else foldl (flip (:)) [] list                        