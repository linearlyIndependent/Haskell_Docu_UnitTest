{-| 
Module      : Convert.Converter
Description : Value converting helper module.
Copyright   : Do whatever you want.
License     : GPL-3
Maintainer  : patrik.ploechl@fake.com
Stability   : Experimental
Portability : POSIX

This module provides functions to convert between different units of measurement.

 * Works with 'Float' as input and output.
 * Accepts positive, negative numbers and 0.
-}
module Convert.Converter
    (
    -- * Convert to mass (kg)
    -- ** From volume (l / dm3)
    kgToDm3
    -- * Convert to volume (l / dm3)
    -- ** From mass (kg)
    , dm3ToKg
    -- * Provides a table which holds the density of common cooking ingredients.
    , densityMap
    ) where  

import qualified Data.Map as Mp
import qualified Data.Maybe as My

{-|
Holds the density of common household cooking ingredients.
Serves as input for the 'densityTable'.
-}
densityBaseTable :: [([Char],Float)]
densityBaseTable = [("water", 1.0),("ice",0.92),("sugar", 1.6),("flour",0.7),("salt",1.6),("butter",0.9),("milk",1.06),("oil",0.9),("alcohol",0.8)]

{-|
Holds the density of common household cooking ingredients.

>>> densityMap
fromList [("alcohol",0.8),("butter",0.9), ...]
-}
densityMap :: Mp.Map [Char] Float
densityMap = Mp.fromList densityBaseTable

{-|
Converts the mass (kg) of the cooking ingredient to it's volume (l / dm3).

 * Returns 'Just' 'Float' if the cooking ingredient is defined in the 'densityMap'.
 * Returns 'Nothing' if the cooking ingredient is not defined in the 'densityMap'.

>>> kgToDm3 "salt" 3.0
"Just 1.875"

>>> kgToDm3 "water" 1.0
"Just 1.0"
-}
kgToDm3 :: [Char] -> Float -> Maybe Float
kgToDm3 name mass = if My.isJust maybeDensity
                    then Just $ mass / (My.fromJust maybeDensity)
                    else Nothing
                    where maybeDensity = Mp.lookup name densityMap

{-|
Converts the volume (l / dm3) of the cooking ingredient to it's mass (kg).

 * Returns 'Just' 'Float' if the cooking ingredient is defined in the 'densityMap'.
 * Returns 'Nothing' if the cooking ingredient is not defined in the 'densityMap'.

>>> kgToDm3 "salt" 3.0
"Just 2.6999998"

>>> kgToDm3 "water" 3.0
"Just 2.76"
-}
dm3ToKg :: [Char] -> Float -> Maybe Float
dm3ToKg name volume = if My.isJust maybeDensity
                      then Just $ volume * (My.fromJust maybeDensity)
                      else Nothing
                      where maybeDensity = Mp.lookup name densityMap