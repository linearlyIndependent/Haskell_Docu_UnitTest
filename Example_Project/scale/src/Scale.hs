{-| 
Module      : Scale
Description : The main scale module.
Copyright   : Do whatever you want.
License     : GPL-3
Maintainer  : patrik.ploechl@fake.com
Stability   : Experimental
Portability : POSIX

This module provides the main Scale-module.
-}
module Scale(-- * Custom types
             KitchenScale(KitchenScale)
             -- ** Lookup functions
           , mass
           , dispMass
             -- * Functions
             -- ** Constructing a new 'KitchenScale'
           , newScale
           , newScaleWith
             -- ** Using a 'KitchenScale'
           , tareScale
           , addIngredient
           ) where

import qualified Convert.Converter as Co
import qualified Scalable.Ingredient as In

import qualified Data.Maybe as My


{-|
A simple kitchen scale.

You can add ingredients, and reset the display.
-}
data KitchenScale =
                    -- | The standard constructor. 
                    KitchenScale { 
                                   mass :: Float -- ^ The mass currently on the scale.
                                 , dispMass :: Float -- ^ The displayed mass.
                                 }
                         deriving (Show)

instance Eq KitchenScale where 
         ks1 == ks2 = (mass ks1) == (mass ks2)

instance Ord KitchenScale where 
         compare ks1 ks2 = compare (mass ks1) (mass ks2)
         
         
{-|
Creates a 'KitchenScale'.

 * Mass and dispMass are both 0.
 
>>> newScale
KitchenScale {mass = 0.0, dispMass = 0.0}
-}
newScale :: KitchenScale
newScale = KitchenScale 0 0

{-|
Creates a 'KitchenScale', with a starting mass and display set to 0.
 
>>> newScaleWith 8.0
KitchenScale {mass = 8.0, dispMass = 0.0}
-}
newScaleWith :: Float -> KitchenScale
newScaleWith startMass
    | startMass > 0 = KitchenScale startMass 0
    | otherwise = newScale

{-|
Sets the display of a 'KitchenScale' to 0.

>>> scale
KitchenScale {mass = 8.0, dispMass = 3.0}
>>> tareScale scale
KitchenScale {mass = 8.0, dispMass = 0.0}
-}
tareScale :: KitchenScale -> KitchenScale
tareScale befScale = KitchenScale (mass befScale) 0

{-|
Adds 'In.CookingIngredient' to the 'KitchenScale'.

>>> scale
KitchenScale {mass = 8.0, dispMass = 3.0}
>>> addIngredient scale $ ingredientWithMass "flour" 0.8
KitchenScale {mass = 8.8, dispMass = 3.8}
-}
addIngredient :: KitchenScale -> In.CookingIngredient -> KitchenScale
addIngredient befScale ingredient = KitchenScale newMass (dispMass befScale + In.mass ingredient)
                              where newMass = (max 0) $ mass befScale + In.mass ingredient

{-
addIngredient :: KitchenScale -> In.CookingIngredient -> KitchenScale
addIngredient befScale ingredient
                            | newMass == 0 = KitchenScale 0 0
                            | otherwise = KitchenScale newMass (dispMass befScale + In.mass ingredient)
                              where newMass = (max 0) $ mass befScale + In.mass ingredient
                              
                              -}